<?php

class Controller{

	protected $data;
	protected $model;
	protected $params;

	/**
	* @return $this->data
	*/
	public function getData(){

		return $this->data;
	}

	/**
	* @return $this->model
	*/
	public function getModel(){

		return $this->model;
	}

	/**
	* @return $this->params
	*/
	public function getParams(){

		return $this->params;
	}

	//The constructor
	public function __construct($data = array()){

		$this->data = $data;
		$this->params = App::getRouter()->getParams();
	}
}